const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

const config = {
    context: path.join(__dirname, 'assets'),
    entry: {
        main: './index.js'
    },
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },

    devtool: 'source-map',

    module: {
        // Special compilation rules
        rules: [
            {
                test: /\.(js|jsx)$/,
                use: {
                    loader: 'babel-loader'
                },
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
                // in that case css in the separate file and hot push does not work need F5
                // but it looks like normal
                //loader: ExtractTextPlugin.extract({
                //    fallback: 'style-loader',
                //    use: {
                //        loader: 'css-loader',
                //    },
               // }),
            },
            {
                test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
                loader: 'url-loader',
                options: {
                    limit: 10000
                }
            }
        ]
    },
    plugins: [
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new HtmlPlugin({
            template: 'index.html'
        })
    ],

    devServer: {
        port: 3000,
        contentBase: './dist',
        hot: true
    }
};

module.exports = config;

// in that case all styles in the html style tag and hot push works properly
/* use: [
'style-loader',
'css-loader'
]
*/
//uc3LBUPVep4H