import React from 'react';
import {Route, Switch} from "react-router";
import NotFound from "./common/NotFound";
import ResultPage from "./pages/ResultPage";
import ArticlePage from "./pages/ArticlePage";

const App = () => (
    <Switch>
        <Route exact path='/fb/results/' component={ResultPage}/>
        <Route exact path='/fb/' component={ArticlePage}/>
        <Route path="*" component={NotFound}/>
    </Switch>
);

export default App