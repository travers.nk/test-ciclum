import React, {Component} from 'react';
import Layout from "../common/Layout";
import ArticleToEdit from "../article/ArticleToEdit";

class ArticlePage extends Component {

    render() {
        return (
            <div>
                <Layout inner={ArticleToEdit} {...this.props}/>
            </div>
        );
    }
}

export default ArticlePage;
