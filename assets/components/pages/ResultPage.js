import React, {Component} from 'react';
import Layout from "../common/Layout";
import ResultForArticle from "../result/ResultForArticle";

class ArticlePage extends Component {

    render() {
        return (
            <div>
                <Layout inner={ResultForArticle} {...this.props}/>
            </div>
        );
    }
}

export default ArticlePage;