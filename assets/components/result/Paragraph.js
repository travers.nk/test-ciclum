import React, { Component } from 'react';
import { connect } from 'react-redux'
import { approveSuggestion, deleteParagraph, clearErrors } from '../../actions';
import { Jumbotron, Button } from 'react-bootstrap';
import Form from "../common/controls/Form";

class Paragraph extends Component {

    constructor (props) {
        super(props);
        this.state = {
            suggestions: []
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    };

    componentDidMount() {
        //this.props.clearErrors();
        this.setState({
            suggestions: this.props.suggestions
        });
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps){
            if (nextProps.suggestions)
                this.setState({
                    suggestions: nextProps.suggestions
                });
        }
    }

    handleSubmit(e){
        e.preventDefault();
        this.props.deleteParagraph(
            {
                articleUrl: localStorage.getItem('articleUrl'),
                originalText: this.props.paragraph
            });
    }

    render() {
        const articleUrl = localStorage.getItem('articleUrl');
        return (
            <Jumbotron>
            <li>
                <div className="clearfix">
                    <Button bsStyle="danger" className="pull-right" type="submit"
                            onClick={this.handleSubmit}>
                        DELETE
                    </Button>
                </div>
                <Form
                    action={this.props.approveSuggestion}
                    formLabel="Original text"
                    formLabelText={this.props.paragraph}
                    inputLabel=""
                    button="APPROVE"
                    actionClear={null}
                    actionClearErrors={this.props.clearErrors}
                    name="text"
                    articleURL={articleUrl}
                    success={false}
                    successCheck={false}
                    suggestions={this.state.suggestions}
                    inputPlaceholder={'Enter your own suggestion'}
                    inputType={'text'}
                    inputName={'text'}
                    buttonStyle={"success"}
                />
            </li>
            <br/>
            </Jumbotron>
        )
    }
}
const mapStateToProps = store => ({
    //errors: store.errors,
});

const mapDispatchToProps = dispatch => ({
    approveSuggestion: (suggestion) => dispatch(approveSuggestion(suggestion)),
    deleteParagraph: (paragraph) => dispatch(deleteParagraph(paragraph)),
    //clearErrors: () => dispatch(clearErrors())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Paragraph);