import React, {Component} from 'react';
import {clearErrors, fetchParagraphs} from "../../actions/index";
import {connect} from "react-redux";
import { Row, Col, Grid } from 'react-bootstrap';
import Paragraph from './Paragraph';


class ResultForArticle extends Component {
    constructor (props) {
        super(props);
        this.state = {
            errors: null,
            paragraphs: null,
            title: 'loading'
        }
    };

    componentDidMount() {
        this.props.clearErrors();
        const articleUrl = localStorage.getItem('articleUrl');
        this.props.fetchParagraphs({
            articleUrl: articleUrl,
            showApproved: this.props.location.search.replace('?', '')
        });
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps){
            this.setState({
                errors: nextProps.errors,
            });
            if (nextProps.paragraphs)
                this.setState({
                    paragraphs: nextProps.paragraphs,
                    title: null
                });
        }
    }

    render() {
        const paragraphs = this.state.paragraphs || [];
        const title = this.state.title;
        return (
            <Grid>
                <Row className="show-grid">
                    { !this.state.errors ?  <h2> {title} </h2> :
                        <Col>
                            <h4>{ this.state.errors.status }</h4>
                            <h4>{ this.state.errors.errors }</h4>
                        </Col>
                    }
                </Row>
                <Row className="show-grid">
                    { paragraphs.length > 0 &&
                    <ul>
                        {paragraphs.map((paragraph, index) => (
                           <Paragraph
                                key={index}
                                paragraph={paragraph._id}
                                suggestions={paragraph.suggestions}
                           />
                        ))}
                    </ul>
                    }
                </Row>
            </Grid>
        );
    }
}

const mapStateToProps = store => ({
    paragraphs: store.paragraphs,
    errors: store.errors,
});

const mapDispatchToProps = dispatch => ({
    fetchParagraphs: (articleURL) => dispatch(fetchParagraphs(articleURL)),
    clearErrors: () => dispatch(clearErrors())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ResultForArticle);