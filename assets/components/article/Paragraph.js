import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Jumbotron } from 'react-bootstrap';
import { addSuggestion, clearSuggestion, clearErrors } from '../../actions'
import Form from "../common/controls/Form";

class Paragraph extends Component {

    constructor (props) {
        super(props);
    };

    render() {
        const articleUrl = localStorage.getItem('articleUrl');
        return (
            <Jumbotron>
            <li>
                <Form
                    action={this.props.addSuggestion}
                    formLabel="ORIGIN TEXT"
                    formLabelText={this.props.paragraph}
                    inputLabel="USERS VERSION"
                    button="SEND CHANGES"
                    actionClear={this.props.clearSuggestion}
                    actionClearErrors={this.props.clearErrors}
                    name="text"
                    articleURL={articleUrl}
                    success={this.props.suggestion ? this.props.suggestion.success : null}
                    successCheck={this.props.suggestion ? this.props.suggestion.result.originalText === this.props.paragraph  : null}
                    suggestions={[]}
                    inputPlaceholder={'Type  your text here'}
                    inputType={'text'}
                    inputName={'text'}
                    buttonStyle={""}
                />
            </li>
            </Jumbotron>
        )
    }
}
const mapStateToProps = store => ({
    suggestion: store.suggestion,
});

const mapDispatchToProps = dispatch => ({
    addSuggestion: (suggestion) => dispatch(addSuggestion(suggestion)),
    clearSuggestion: () => dispatch(clearSuggestion()),
    clearErrors: () => dispatch(clearErrors())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Paragraph);