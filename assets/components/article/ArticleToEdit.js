import React, {Component} from 'react';
import { connect } from 'react-redux';
import { fetchArticle, clearErrors} from '../../actions';
import { Row, Col, Grid } from 'react-bootstrap';
import Paragraph from './Paragraph';

class ArticleToEdit extends Component {
    constructor (props) {
        super(props);
        this.state = {
            errors: null,
            title: null,
            paragraphs: null,
            articleUrl: null
        }
    };

    componentDidMount() {
        this.props.clearErrors();
        this.props.fetchArticle(this.props.location.search);
        localStorage.setItem('articleUrl', this.props.location.search)
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps){
            this.setState({
                errors: nextProps.errors,
            });
            if (nextProps.article)
                this.setState({
                    title:  nextProps.article.result.title,
                    paragraphs: nextProps.article.result.paragraphs,
                    articleUrl: nextProps.article.result.articleUrl
                });
        }
    }

    render() {
        const paragraphs = this.state.paragraphs || [];
        const title = this.state.title || 'loading';
        return (
            <Grid>
                <Row className="show-grid">
                { !this.state.errors ?  <h2> {title} </h2> :
                    <Col>
                        <h4>{ this.state.errors.status }</h4>
                        <h4>{ this.state.errors.errors }</h4>
                    </Col>
                }
                </Row>

                <Row className="show-grid">
                { paragraphs.length > 0 &&
                    <ul>
                        {paragraphs.map((paragraph, index) => (

                            <Paragraph
                                key={index}
                                paragraph={paragraph}
                            />

                        ))}
                    </ul>
                }
                </Row>
            </Grid>
        );
    }
}

const mapStateToProps = store => ({
    article: store.article,
    errors: store.errors,
});

const mapDispatchToProps = dispatch => ({
    fetchArticle: (articleURL) => dispatch(fetchArticle(articleURL)),
    clearErrors: () => dispatch(clearErrors())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ArticleToEdit);