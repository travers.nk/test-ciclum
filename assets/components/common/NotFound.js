import React from "react";
import { Jumbotron } from 'react-bootstrap';

class NotFound extends React.Component {
    render() {
        return (
            <Jumbotron>
                <h2>404</h2>
                <p>Not Found</p>
            </Jumbotron>
        );
    }
}

export default NotFound;