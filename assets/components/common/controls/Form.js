import React, { Component } from 'react';
import {FormGroup, ControlLabel, Button, Label} from 'react-bootstrap';
import Input from "./Input";
import SimpleForm from "./SimpleForm";

class Form extends Component {

    constructor (props) {
        super(props);
        this.state = {
            text: '',
            success: false,
            errors: null,
            suggestions: []
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleClearForm = this.handleClearForm.bind(this);
    };

    componentDidMount() {
        if (this.props.suggestions.length > 0)
            this.setState({
                suggestions: this.props.suggestions,
            });
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps){
            this.setState({
                success: false,
            });
            if (nextProps.success && nextProps.successCheck)
                this.setState({
                    success: true,
                });
            if (nextProps.suggestions){
                this.setState({
                    suggestions: nextProps.suggestions,
                });
            }
        }
    }

    handleInputChange(e) {
        e.preventDefault();

        if(this.props.actionClear) this.props.actionClear();

        this.setState({
            text: e.target.value
        });
    };

    handleSubmit(e){
        e.preventDefault();
        this.props.action(
            {
                articleUrl: this.props.articleURL,
                originalText: this.props.formLabelText,
                usersText: this.state.text,
            });
        this.handleClearForm(e);
    }

    handleClearForm(e) {
        e.preventDefault();
        this.setState({
            text: '',
        });
    }

    render() {
        const showSuggestions = this.state.suggestions.length > 0,
           suggestions = this.state.suggestions;

        return (
            <form>
                <FormGroup controlId="name">
                    <ControlLabel> {this.props.formLabel} </ControlLabel>
                    <p>{this.props.formLabelText}</p>
                    { showSuggestions &&
                        <div>
                            <ControlLabel> User suggestions </ControlLabel>
                            <ul>
                                {suggestions.map((suggestion, index) => (
                                    <SimpleForm
                                        key={index}
                                        text={suggestion.usersText}
                                        originalText={this.props.formLabelText}
                                        id={suggestion._id}
                                        action={this.props.action}
                                        button="APPROVE"
                                    />
                                ))}
                            </ul>
                        </div>
                    }
                    <p className="pull-right success">
                        {this.state.success &&	<Label bsStyle="success">Success</Label>}
                    </p>
                    <Input
                        inputType={this.props.inputType}
                        title={this.props.inputLabel}
                        name={this.props.inputName}
                        controlFunc={this.handleInputChange}
                        content={this.state.text}
                        placeholder={this.props.inputPlaceholder} />
                </FormGroup>
                <div className="clearfix">
                    <Button  bsStyle={this.props.buttonStyle} className="pull-right" type="submit" onClick={this.handleSubmit}>
                        {this.props.button}
                    </Button>
                </div>
            </form>
        )
    }
}
export default Form;