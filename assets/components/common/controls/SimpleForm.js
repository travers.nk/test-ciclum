import React, { Component } from 'react';
import { Button } from 'react-bootstrap';


class SimpleForm extends Component {

    constructor (props) {
        super(props);
        this.state = {
            text: null,
            id: null,
            success: false,
            errors: null,
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    };

    componentDidMount() {
        this.setState({
            text: this.props.text,
            id: this.props.id,
        });
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps){
            this.setState({
                text: nextProps.text,
                id:  nextProps.id,
            });
        }
    }

    handleSubmit(e){
        e.preventDefault();
        this.props.action(
            {
                id: this.state.id,
                usersText: this.state.text,
                originalText: this.props.originalText,
                isApproved: true
            });
    }

    render() {
        return (
            <li>
                <form className="form-inline">
                    <div className="clearfix">
                        <p className="pull-left">
                            {this.state.text}
                        </p>
                        <Button  bsStyle="success" className="pull-right" type="submit" onClick={this.handleSubmit}>
                            {this.props.button}
                        </Button>
                    </div>
                </form>
            </li>
        )
    }
}
export default SimpleForm;