import React, {Component} from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';

class NavBar extends Component {
    constructor(props) {
        super(props);
    }


    render(){
        return (
            <Navbar>
                <Navbar.Header>
                    <Navbar.Brand>
                        <span>Ciclum</span>
                    </Navbar.Brand>
                </Navbar.Header>
            </Navbar>
        )
    }
}

export default NavBar;