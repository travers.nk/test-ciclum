const suggestion = (store = null, action) => {
    switch (action.type) {
        case 'ADD_SUGGESTION_SUCCESS':
            return action.payload;
        case 'CLEAR_SUGGESTION':
            return null;
        default:
            return store;
    }
};

export default suggestion;

