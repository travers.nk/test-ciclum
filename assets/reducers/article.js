const article = (store = null, action) => {
    switch (action.type) {
        case 'FETCH_ARTICLE_SUCCESS':
            return action.payload;
        default:
            return store;
    }
};

export default article;