import {combineReducers} from 'redux';
import article from "./article";
import paragraphs from "./paragraph";
import suggestion from "./suggestion";
import errors from "./errors";


const app = combineReducers({
    paragraphs,
    suggestion,
    article,
    errors
});

export default app
