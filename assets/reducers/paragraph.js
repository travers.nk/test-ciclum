const paragraphs = (store = null, action) => {
    switch (action.type) {

        case 'FETCH_PARAGRAPHS_SUCCESS':
            return action.payload;

        case 'APPROVE_SUGGESTION_SUCCESS':
            const newStore = Object.assign([], store);
            const indexParagraph = store.findIndex(paragraph => {
                    return String(paragraph._id) === String(action.payload.result.originalText);
                });
            const suggestions = newStore[indexParagraph]['suggestions'];
            const indexSuggestion = suggestions.findIndex(suggestion => {
                return suggestion._id === action.payload.result._id
                }
            );
            if (indexSuggestion >= 0){
                suggestions.splice(indexSuggestion, 1);
                newStore[indexParagraph]['suggestions'] = Object.assign([], suggestions);
            }
            return newStore;

        case 'DELETE_PARAGRAPH_SUCCESS':
            const indexParagraphP = store.findIndex(paragraph => {
                return String(paragraph._id) === String(action.payload);
            });
            const newStorePP = [];
            for (let index in store){
                let item  = JSON.parse(JSON.stringify(store[index]));
                newStorePP.push(item)
            }
            newStorePP.splice(indexParagraphP, 1);
            return newStorePP;

        default:
            return store;
    }
};

export default paragraphs;