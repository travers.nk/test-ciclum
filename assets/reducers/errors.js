const errors = (store = null, action) => {
    switch (action.type) {
        case 'ADD_SUGGESTION_FAIL':
            return action.payload;
        case 'FETCH_PARAGRAPHS_FAIL':
            return action.payload;
        case 'DELETE_PARAGRAPH_FAIL':
            return action.payload;
        case 'APPROVE_SUGGESTION_FAIL':
            return action.payload;
        case 'FETCH_ARTICLE_FAIL':
            return action.payload;
        case 'CLEAR_ERRORS':
            return null;
        default:
            return store;
    }
};

export default errors;