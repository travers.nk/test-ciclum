import http from "../../lib/api";
import config from "../config.json";

const fetchArticleSuccess = article => ({
    type: 'FETCH_ARTICLE_SUCCESS',
    payload: article
});

const fetchArticleFail = error => ({
    type: 'FETCH_ARTICLE_FAIL',
    payload: error
});

export const fetchArticle = articleURL => dispatch =>
    http(config.url + articleURL)
        .get()
        .then(article => {
            dispatch(fetchArticleSuccess(article))
        })
        .catch(err => {
            dispatch(fetchArticleFail(err));
        });

const fetchParagraphsSuccess = paragraphs => ({
    type: 'FETCH_PARAGRAPHS_SUCCESS',
    payload: paragraphs
});

const fetchParagraphsFail = error => ({
    type: 'FETCH_PARAGRAPHS_FAIL',
    payload: error
});

export const fetchParagraphs = params => dispatch =>
    http(config.url + '/paragraph' + params['articleUrl'] + '&' + params['showApproved'])
        .get()
        .then(paragraphs => {
            dispatch(fetchParagraphsSuccess(paragraphs))
        })
        .catch(err => {
            dispatch(fetchParagraphsFail(err));
        });

const addSuggestionSuccess = result => ({
    type: 'ADD_SUGGESTION_SUCCESS',
    payload: result,
});

const addSuggestionFail = error => ({
    type: 'ADD_SUGGESTION_FAIL',
    payload: error,
});

export const addSuggestion = data => dispatch =>
    http(config.url)
        .post(data)
        .then(result => {
            dispatch(addSuggestionSuccess(result));
        })
        .catch(err => {
            dispatch(addSuggestionFail(err));
        });

const deleteParagraphSuccess = paragraph => ({
    type: 'DELETE_PARAGRAPH_SUCCESS',
    payload: paragraph,
});

const deleteParagraphFail = error => ({
    type: 'DELETE_PARAGRAPH_FAIL',
    payload: error,
});

export const deleteParagraph = paragraph => dispatch =>
    http(config.url + '/paragraph')
        .delete(paragraph)
        .then(result => {
            dispatch(deleteParagraphSuccess(paragraph.originalText));
        })
        .catch(err => {
            dispatch(deleteParagraphFail(err));
        });

const approveSuggestionSuccess = result => ({
    type: 'APPROVE_SUGGESTION_SUCCESS',
    payload: result,
});

const approveSuggestionFail = error => ({
    type: 'APPROVE_SUGGESTION_FAIL',
    payload: error,
});

export const approveSuggestion = data => dispatch =>
    http(config.url + '/paragraph')
        .put(data)
        .then(result => {
            dispatch(approveSuggestionSuccess(result));
        })
        .catch(err => {
            dispatch(approveSuggestionFail(err));
        });

export const clearSuggestion = () => {
    return {
        type: 'CLEAR_SUGGESTION',
    }
};

export const clearErrors = () => {
    return {
        type: 'CLEAR_ERRORS',
    }
};