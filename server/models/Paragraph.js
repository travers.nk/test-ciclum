const mongoose = require('mongoose'),
    Schema = mongoose.Schema;


const ParagraphSchema = new Schema({
    articleUrl: {
        type: String,
        required: true,
    },

    originalText: {
        type: String,
    },

    usersText: {
        type: String,
    },

    isApproved: {
        type: Boolean,
        default: false
    }
});


module.exports = mongoose.model('Paragraph', ParagraphSchema);