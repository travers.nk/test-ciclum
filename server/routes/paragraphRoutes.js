/**
 * Created by nataly on 20.11.17.
 */
const router = require('express').Router();
const paragraphController = require('../controllers/ParagraphController');

router.route('/')
    .get(paragraphController.index)
    .post(paragraphController.create);

router.route('/paragraph')
    .get(paragraphController.indexParagraphs)
    .put(paragraphController.updateParagraphs)
    .delete(paragraphController.deleteParagraphs);


module.exports = router;