/**
 * Created by nataly on 20.11.17.
 */
const paragraphRoutes = require('./paragraphRoutes');
const router = require('express').Router();
router.use('/fb', paragraphRoutes);

module.exports = router;
