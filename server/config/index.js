/**
 * Created by nataly on 07.12.17.
 */
const nconf = require('nconf');
const path = require('path');

nconf.argv()
    .env()
    .file({file: path.join(__dirname, 'config.json')});

module.exports = nconf;