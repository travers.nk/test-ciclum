/**
 * Created by nataly on 07.12.17.
 */

const createError = require('http-errors');
const _ = require('lodash');
const mongoose = require('mongoose');
const ValidationError = mongoose.Error.ValidationError;
const ValidatorError  = mongoose.Error.ValidatorError;


const errors = {
    400: new createError.BadRequest(["Url is not defined"]),
    404: new createError.NotFound(["Article not found"]),
    500: new createError.InternalServerError(),
    501: new createError.NotImplemented(['Database is not connected']),
    401: new createError.Unauthorized(['You should login']),
    403: new createError.Forbidden(['You do not have access']),
    406: new createError.NotAcceptable()
};

const sendHttpError = function(err, res){
    err.status ? res.status(err.status) : res.status(503);
    res.json(err);
};

function checkInstance(err){
    if (err instanceof createError.HttpError) return 'http';
    if (err instanceof ValidationError || err instanceof ValidatorError ) return 'validation';
    if (err.name === 'MongoError' && err.code === 11000) return 'unique';
    return 'unknown'
}

module.exports = function (app) {
    return function (err, req, res, next){
        console.log(err);
        if (typeof err === 'number') {
            err = errors[err];
        }
        const errType = checkInstance(err);

        switch (errType){
            case 'http':
                sendHttpError(err, res);
                break;
            case 'validation':
                sendHttpError(createError.NotAcceptable(_.map(err.errors, 'message')), res);
                break;
            case 'unique':
                sendHttpError(createError.NotAcceptable([err.msg]), res);
                break;
            default:
                if (app.get('env') === 'development') {
                    next(err);
                } else {
                    if (err){
                        sendHttpError(err, res);
                    } else {
                        sendHttpError(createError.ServiceUnavailable(['Oops, some-thing wrong with service']), res);
                    }
                }
                break;
        }
    }
};