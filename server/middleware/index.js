module.exports = function (app, express) {

    const path = require('path'),
    bodyParser = require('body-parser'),
    favicon = require('serve-favicon'),
    errorHandler = require('./error_handler')(app),
    mongoose = require('mongoose');

    mongoose.Promise = global.Promise;
    let dbReady = false;

    mongoose.connect('mongodb://localhost/test',  {
        useMongoClient: true,
    }).then(
        () => {
            console.log('MongoDB connected');
            dbReady = true;
            },
        (err) => {
        }
    );

    app.use((req, res, next) => {
        if (dbReady) {
            return next();
        }
        next(501);
    });

    require('../models/Paragraph');

    const routes = require('../routes/indexRoutes');

    app.use(favicon(path.join(__dirname, '../public', 'favicon.ico')));
    app.use('/fb', express.static(path.join(__dirname, '../../dist')));

    app.use((req, res, next) => {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
        next();
    });

    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json()); // Body parser use JSON data

    app.use('/api/v1/', routes);
    app.get('/fb*', (req, res) => {
        res.sendFile(path.resolve(__dirname, '../../', 'dist', 'index.html'));
    });
    app.use(errorHandler);
}