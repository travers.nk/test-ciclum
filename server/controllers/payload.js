const payload = function getPayload(req) {
    let payload = {};
    for (const key in req.body){
        if (key === 'articleUrl'){
            payload[key] = req.body[key].trim().replace('?articleURL=', '');
        }
        else {
            try {
                payload[key] = req.body[key].trim();
            }
            catch (e){
                payload[key] = req.body[key];
            }
        }
    }
    return payload
};

module.exports = payload;