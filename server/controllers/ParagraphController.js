/**
 * Created by nataly on 20.11.17.
 */
const mongoose = require('mongoose'),
    Paragraph = mongoose.model('Paragraph'),
    getPayload = require('./payload'),
    getAndParse = require('./helper');

class ParagraphController {
    async index(req, res, next) {
        let articleURL = req.query.articleURL;

        if (articleURL){
             const data = await getAndParse(articleURL, next);

             if (!data.success){
                 next(data.result);
             } else {

                 if (data.result.paragraphs.length === 0 && !data.result.title)
                     next(404);
                 else res.status(200).json(data);
             }
        } else {
             next(400)
        }
    }

    async indexParagraphs(req, res, next) {
        const articleURL = req.query.articleURL,
            isApproved = req.query.showApproved === 'true';
        let queryObject =  {
            $match: {articleUrl: articleURL}
        };

        if (articleURL && isApproved){
            const queryB = {
                articleUrl: articleURL,
                isApproved: isApproved
            };
            const approvedParagraphs = await Paragraph.distinct('originalText', queryB);
            queryObject =  {
                $match: {originalText: {$in: approvedParagraphs }}
            };
        }

        if (articleURL){
                Paragraph.aggregate([
                    queryObject,
                    {
                        $group: {
                            _id: "$originalText",
                            suggestions: {$push: "$$ROOT"},
                        }
                    }
                ], function (err, data) {

                    if (err) {
                        next(err);
                    } else {
                        if (data.length <= 0) {
                            next(404);
                        } else res.status(200).json(data);
                    }
                });
        } else {
            next(400)
        }
    }

    async updateParagraphs(req, res, next) {
        const payload = getPayload(req);

        if (payload){
            try{
                await Paragraph.update(
                    {originalText: payload['originalText']},
                    {$set: { isApproved: false }},
                    {multi: true}
                    );
            }
            catch(e){
                console.log(e)
            }

            if (payload['id']){
                Paragraph.findOneAndUpdate(
                    {_id: payload['id']},
                    {isApproved: payload['isApproved']},
                    {new: true}, (err, paragraph) => {
                        if (err)
                            next(err);
                        else
                            res.status(200).json({
                                success: true,
                                result: paragraph
                            });
                    });

            } else {
                payload['isApproved'] = true;
                let  newParagraph = new Paragraph(payload);
                newParagraph.save((err, paragraph) => {

                    if (err)
                        next(err);
                    else {
                        res.status(201).json({
                            success: true,
                            result: paragraph
                        });
                    }
                });
            }
        } else {
            next(400)
        }
    }

    create(req, res, next){
        const payload = getPayload(req);
        let  newParagraph = new Paragraph(payload);
        newParagraph.save((err, paragraph) => {

            if (err)
                next(err);
                else {
                res.status(201).json({
                    success: true,
                    result: paragraph
                });
            }
        });
    }

    deleteParagraphs(req, res, next){
        const payload = getPayload(req);

        if (payload) {
                Paragraph.remove(
                    {
                        originalText: payload['originalText'],
                        articleUrl: payload['articleUrl']
                    },
                    (err, result) => {

                        if (err){
                            next(err);
                        }
                        else {
                            res.status(204).json({
                                success: true,
                                result: result
                            });
                        }
                    }
                );
        } else next(400)
    }
}

module.exports = new ParagraphController();


//articleURL = 'https://www.dagbladet.no/kultur/utvidelse-av-kampsonen/69351526';
//​https://www.dagbladet.no/kjendis/supermodellen-ble-beskyldt-for-a-ikke-tipse-etter-et-barbesok-na-svarer-hun-pa-kritikken/68573788