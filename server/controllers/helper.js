
const needle = require('needle'),
      cheerio = require('cheerio');

getAndParse = async function(articleUrl){
   try {
        const response = await needle('get', articleUrl);

        if (Buffer.isBuffer(response.body) && response.body.length === 0 || response.body.length === 0 )
            return {success: false, result: 404}
        else {
            let result = {},
                paragraphs = [],
                $ = cheerio.load(response.body.toString('utf8'));
            result['title'] = $('h2.headline').text();
            result['articleUrl'] =  articleUrl;
            $('.body-copy p').each(function() {
                paragraphs.push($(this).text());
            });
            result['paragraphs'] = paragraphs;
            return {success: true, result: result}
        }
   }
    catch(e){
        return {success: false, result: e}
   }
};

module.exports = getAndParse;